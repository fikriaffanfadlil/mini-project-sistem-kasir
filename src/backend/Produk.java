package backend;

import java.util.ArrayList;
import java.util.Queue;
import java.util.LinkedList;

public class Produk {

    Queue<ItemProduk> daftarItem = new LinkedList<>();
    String nama;
    int hargaJual;
    int penjualan;
    int totalHPP;

    public Produk(String nama, int hargaJual) {
        this.nama = nama;
        this.hargaJual = hargaJual;
    }

    //Methods
    public void tambahkanItem(ItemProduk item) {
        this.daftarItem.add(item);
    }

    //
    public void jualItem(int kuantitas) {
        for (int i = 0; i < kuantitas; i++) {
            totalHPP += daftarItem.poll().getHPP();
            penjualan += hargaJual;
        }
    }

    public double getAvgHPP() {
        //Membuat array HPP
        ArrayList<Integer> arrayHPP = new ArrayList<>();
        for (ItemProduk item: daftarItem) arrayHPP.add(item.getHPP());

        //Menghitung dari array HPP
        Integer jumlah = 0;
        if(!arrayHPP.isEmpty()) {
            for (Integer HPP : arrayHPP) {
                jumlah += HPP;
            }
            return jumlah.doubleValue() / arrayHPP.size();
        }
        return jumlah;
    }

    //Other Getter & Setter
    public Queue<ItemProduk> getDaftarItem() {
        return daftarItem;
    }
    public String getNama() {
        return nama;
    }
    public int getHargaJual() {
        return hargaJual;
    }
    public int getPenjualan() {
        return penjualan;
    }
    public int getTotalHPP() {
        return totalHPP;
    }
    public void setHargaJual(int hargaJual) {
        this.hargaJual = hargaJual;
    }
}

