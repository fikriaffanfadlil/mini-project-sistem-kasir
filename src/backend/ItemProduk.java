package backend;

public class ItemProduk {
    private Produk produk;
    private int HPP; //HPP = Harga Pokok Penjualan

    public ItemProduk(Produk produk, int HPP){
        this.produk = produk;
        this.HPP = HPP;
    }

    public int getHPP() {
        return HPP;
    }
    public Produk getProduk() {
        return produk;
    }
}