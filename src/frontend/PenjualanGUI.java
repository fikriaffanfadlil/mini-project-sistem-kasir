package frontend;

import backend.Produk;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class PenjualanGUI {

    public PenjualanGUI(JFrame frame, ArrayList<Produk> daftarProduk) {
        JPanel mainPanel = new JPanel();

        //Labels & TextFields
        JLabel titleLabel = new JLabel("Penjualan");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemKasirGUI.fontTitle);

        JLabel pilihLabel = new JLabel("Pilih produk",SwingConstants.CENTER);
        pilihLabel.setFont(SistemKasirGUI.fontGeneral);

        JLabel stokLabel = new JLabel("Stok: " + 0,SwingConstants.CENTER);
        stokLabel.setFont(SistemKasirGUI.fontGeneral);

        //Drop down Nama Produk
        ///Membuat list nama produk
        ArrayList<String> arrayNama = new ArrayList<>();
        for (Produk m: daftarProduk) arrayNama.add(m.getNama());
        ///Setting up combo box
        JComboBox<String> comboBox = new JComboBox<>();
        comboBox.setModel(new DefaultComboBoxModel<>(arrayNama.toArray(new String[0])));
        comboBox.setSelectedItem(null);
        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (comboBox.getSelectedItem()!=null) {
                    Produk produk = ManajemenPersediaanGUI.getProduk(daftarProduk, comboBox.getSelectedItem().toString());
                    stokLabel.setText("Stok: " + produk.getDaftarItem().size());
                }
            }
        });

        JLabel kuantitasLabel = new JLabel("Kuantitas penjualan:",SwingConstants.CENTER);
        kuantitasLabel.setFont(SistemKasirGUI.fontGeneral);

        JTextField kuantitasTf = new JTextField();

        //Buttons
        JButton buttonJual = new JButton("Jual");
        buttonJual.setBackground(Color.green);
        buttonJual.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (comboBox.getSelectedItem()==null || kuantitasTf.getText().equals("")) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }
                else {
                    Produk produk = ManajemenPersediaanGUI.getProduk(daftarProduk, comboBox.getSelectedItem().toString());
                    int kuantitas = Integer.parseInt(kuantitasTf.getText());
                    //Memastikan kuantitas penjualan lebih besar 0
                    if (kuantitas <= 0){
                        JOptionPane.showMessageDialog(frame, "Kuantitas penjualan harus bernilai lebih besar dari 0");
                        kuantitasTf.setText("");
                    }
                    //Memastikan stok mencukupi
                    else if ((produk.getDaftarItem().size() - kuantitas < 0)) {
                        JOptionPane.showMessageDialog(frame, "Stok produk tidak mencukupi");
                        kuantitasTf.setText("");
                    }
                    else {
                        //Menjual produk
                        produk.jualItem(kuantitas);
                        JOptionPane.showMessageDialog(frame, kuantitasTf.getText() + " " + produk.getNama()+" berhasil dijual");
                        kuantitasTf.setText("");
                        stokLabel.setText("Stok: " + produk.getDaftarItem().size());
                        frame.repaint();
                    }
                }
            }
        });

        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.setBackground(Color.pink);
        buttonKembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(mainPanel);
                new ManajemenPersediaanGUI(frame, daftarProduk);
            }
        });


        mainPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;

        gbc.insets = new Insets(-70,-30,0,-30);
        mainPanel.add(titleLabel, gbc);

        gbc.insets = new Insets(2,0,0,0);
        mainPanel.add(pilihLabel, gbc);
        gbc.insets = new Insets(2,0,20,0);
        mainPanel.add(comboBox, gbc);
        mainPanel.add(stokLabel, gbc);

        gbc.insets = new Insets(2,0,0,0);
        mainPanel.add(kuantitasLabel, gbc);
        mainPanel.add(kuantitasTf, gbc);

        gbc.insets = new Insets(20,10,0,10);
        mainPanel.add(buttonJual, gbc);

        gbc.insets = new Insets(15,10,-40,10);
        mainPanel.add(buttonKembali, gbc);

        frame.add(mainPanel);
        frame.repaint();
        frame.setVisible(true);

    }
}
