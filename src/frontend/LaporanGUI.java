package frontend;

import backend.Produk;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class LaporanGUI {
    public LaporanGUI(JFrame frame, ArrayList<Produk> daftarProduk) {
        //Menghitung Keuntungan total dari semua produk
        double pendapatan = 0;
        double HPP = 0;
        for (Produk produk: daftarProduk) {
            pendapatan += produk.getPenjualan();
            HPP += produk.getTotalHPP();
        }
        double keuntungan = pendapatan - HPP;

        //Membuat panel
        JPanel mainPanel = new JPanel();
        //Komponen-komponen
        JLabel titleLabel = new JLabel("Laporan Laba Rugi");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemKasirGUI.fontTitle);

        JLabel metodeLabel = new JLabel("Metode perhitungan: Perpetual-FIFO");
        metodeLabel.setFont(new Font("Century Gothic", Font.ITALIC, 12));

        JLabel pendapatanLabel = new JLabel(String.format("Total Pendapatan: Rp%,.2f", pendapatan), SwingConstants.CENTER);
        pendapatanLabel.setFont(SistemKasirGUI.fontGeneral);
        JLabel HPPLabel = new JLabel(String.format("Total HPP: Rp%,.2f", HPP), SwingConstants.CENTER);
        HPPLabel.setFont(SistemKasirGUI.fontGeneral);
        JLabel keuntunganLabel = new JLabel(String.format("Total Keuntungan: Rp%,.2f", keuntungan), SwingConstants.CENTER);
        keuntunganLabel.setFont(SistemKasirGUI.fontGeneral);

        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.setBackground(Color.pink);
        buttonKembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(mainPanel);
                new HomeGUI(frame, daftarProduk);
            }
        });

        //Menambahkan komponen pada panel dan mengatur layout
        mainPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;

        gbc.insets = new Insets(30,0,0,0);
        mainPanel.add(titleLabel, gbc);

        gbc.insets = new Insets(0,10,30,10);
        mainPanel.add(metodeLabel, gbc);

        gbc.insets = new Insets(2,10,0,10);
        mainPanel.add(pendapatanLabel, gbc);
        mainPanel.add(HPPLabel, gbc);
        mainPanel.add(keuntunganLabel, gbc);

        gbc.insets = new Insets(30,10,30,10);
        mainPanel.add(buttonKembali, gbc);


        frame.add(mainPanel);
        frame.repaint();
        frame.setVisible(true);
    }
}
