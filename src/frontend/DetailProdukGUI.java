package frontend;

import backend.ItemProduk;
import backend.Produk;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class DetailProdukGUI {
    Produk produk;
    JFrame frame;

    public DetailProdukGUI(Produk produk, JFrame frame, ArrayList<Produk> daftarProduk) {
        this.produk = produk;
        this.frame = frame;

        JPanel mainPanel = new JPanel();

        JLabel titleLabel = new JLabel("Detail Produk \""+produk.getNama()+"\"");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemKasirGUI.fontTitle);

        //Komponen-komponen

        JLabel hargaJualLabel = new JLabel(String.format("Harga jual produk: Rp%,.2f", (double) produk.getHargaJual()),SwingConstants.CENTER);
        hargaJualLabel.setFont(SistemKasirGUI.fontGeneral);

        JTextField hargaJualTf = new JTextField();


        //Menentukan stok sesuai size dari Queue daftarItem
        JLabel stokLabel = new JLabel( String.format("Jumlah Stok : %d | Rata-rata biaya per unit: Rp%,.2f", produk.getDaftarItem().size(), produk.getAvgHPP()), SwingConstants.CENTER);
        stokLabel.setFont(SistemKasirGUI.fontGeneral);

        JLabel addStokLabel = new JLabel("Kuantitas penambahan stok: ",SwingConstants.CENTER);
        addStokLabel.setFont(SistemKasirGUI.fontGeneral);

        JTextField addStokTf = new JTextField();

        JLabel biayaPembelianLabel = new JLabel("Biaya pembelian per unit (Rp): ",SwingConstants.CENTER);
        biayaPembelianLabel.setFont(SistemKasirGUI.fontGeneral);

        JTextField biayaPembelianTf = new JTextField();

        //Buttons
        JButton buttonTambahkanStok = new JButton("Tambahkan Stok");
        buttonTambahkanStok.setBackground(Color.green);
        buttonTambahkanStok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Memastikan textField tidak kosong
                if (addStokTf.getText().equals("")|| biayaPembelianTf.getText().equals("")) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi kedua Field di atas");
                }
                else {
                    //Instansiasi itemProduk sebanyak [Kuantitas Penambahan Stok] kali, lalu masukan ke dalam produk
                    for (int i = 0; i < Integer.parseInt(addStokTf.getText()); i++) {
                        produk.tambahkanItem(new ItemProduk(produk, Integer.parseInt(biayaPembelianTf.getText())));
                    }
                    stokLabel.setText(String.format("Jumlah stok : %d | Rata-rata biaya per unit: Rp%,.2f", produk.getDaftarItem().size(), produk.getAvgHPP()));
                    frame.repaint();
                    JOptionPane.showMessageDialog(frame, addStokTf.getText() + " unit " + produk.getNama() + " dengan harga pokok penjualan Rp" + biayaPembelianTf.getText() + " berhasil ditambahkan");
                }

                //Mengosongkan textField
                addStokTf.setText("");
                biayaPembelianTf.setText("");
            }
        });
        //Buttons
        JButton buttonGantiHargaJual = new JButton("Ganti Harga Jual");
        buttonGantiHargaJual.setBackground(Color.green);
        buttonGantiHargaJual.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Memastikan textField tidak kosong
                if (hargaJualTf.getText().equals("")) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi Field di atas");
                }
                //Ganti harga jual dengan method dari class Produk
                else {
                    produk.setHargaJual(Integer.parseInt(hargaJualTf.getText()));
                    hargaJualLabel.setText("Harga jual produk: Rp"+ produk.getHargaJual());
                    hargaJualLabel.repaint();
                    JOptionPane.showMessageDialog(frame, "Berhasil mengganti harga jual " + produk.getNama() + " menjadi " + hargaJualTf.getText());
                }

                //Mengosongkan textField
                hargaJualTf.setText("");
            }
        });



        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.setBackground(Color.pink);
        buttonKembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(mainPanel);
                new ManajemenPersediaanGUI(frame, daftarProduk);
            }
        });


        mainPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(30,0,30,0);
        mainPanel.add(titleLabel, gbc);

        gbc.insets = new Insets(2,10,0,10);
        mainPanel.add(hargaJualLabel, gbc);
        mainPanel.add(hargaJualTf, gbc);
        gbc.insets = new Insets(5,40,50,40);
        mainPanel.add(buttonGantiHargaJual, gbc);

        gbc.insets = new Insets(2,10,0,10);
        mainPanel.add(stokLabel, gbc);
        mainPanel.add(addStokLabel, gbc);
        mainPanel.add(addStokTf,gbc);
        mainPanel.add(biayaPembelianLabel, gbc);
        mainPanel.add(biayaPembelianTf, gbc);
        gbc.insets = new Insets(5,40,20,40);
        mainPanel.add(buttonTambahkanStok, gbc);

        gbc.insets = new Insets(30,40,30,40);
        mainPanel.add(buttonKembali, gbc);



        frame.add(mainPanel);
        frame.repaint();
        frame.setVisible(true);
    }
}
