package frontend;

import backend.Produk;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class ProdukBaruGUI {
    public ProdukBaruGUI(JFrame frame, ArrayList<Produk> daftarProduk) {
        JPanel mainPanel = new JPanel();

        //Labels & TextFields
        JLabel titleLabel = new JLabel("Tambah Produk Baru");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemKasirGUI.fontTitle);

        JLabel namaLabel = new JLabel("Nama Produk:",SwingConstants.CENTER);
        namaLabel.setFont(SistemKasirGUI.fontGeneral);

        JTextField namaTf = new JTextField();

        JLabel hargaJualLabel = new JLabel("Harga Jual Produk (Rp):",SwingConstants.CENTER);
        hargaJualLabel.setFont(SistemKasirGUI.fontGeneral);

        JTextField hargaJualTf = new JTextField();

        //Buttons
        JButton buttonTambahkan = new JButton("Tambahkan");
        buttonTambahkan.setBackground(Color.green);
        buttonTambahkan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (namaTf.getText().equals("")|| hargaJualTf.getText().equals("")) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }
                //Mengecek jika Produk sudah ada dalam daftar
                else if (daftarProduk.stream().map(Produk::getNama).collect(Collectors.toList()).contains(namaTf.getText())) {
                    JOptionPane.showMessageDialog(frame, "Produk "+namaTf.getText()+"sudah ada dalam daftar");
                }
                else {
                    //Instansiasi Produk
                    daftarProduk.add(new Produk(namaTf.getText(), Integer.parseInt(hargaJualTf.getText())));
                    JOptionPane.showMessageDialog(frame, "Produk "+namaTf.getText()+" berhasil ditambahkan");
                    namaTf.setText("");
                    hargaJualTf.setText("");
                }
            }
        });

        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.setBackground(Color.pink);
        buttonKembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(mainPanel);
                new ManajemenPersediaanGUI(frame, daftarProduk);
            }
        });


        mainPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(-70,-30,0,-30);

        mainPanel.add(titleLabel, gbc);
        gbc.insets = new Insets(10,0,0,0);
        mainPanel.add(namaLabel, gbc);
        mainPanel.add(namaTf,gbc);
        gbc.insets = new Insets(5,0,0,0);
        mainPanel.add(hargaJualLabel, gbc);
        mainPanel.add(hargaJualTf, gbc);

        gbc.insets = new Insets(30,10,-10,10);
        mainPanel.add(buttonTambahkan, gbc);
        gbc.insets = new Insets(15,10,-40,10);
        mainPanel.add(buttonKembali, gbc);

        frame.add(mainPanel);
        frame.repaint();
        frame.setVisible(true);

    }

}
