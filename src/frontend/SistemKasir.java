package frontend;

import backend.Produk;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class SistemKasir {
    public static void main(String[] args) {
        new SistemKasirGUI();
    }
}

class SistemKasirGUI extends JFrame {
    private static ArrayList<Produk> daftarProduk = new ArrayList<>();

    public static Font fontGeneral = new Font("Century Gothic", Font.PLAIN, 14);
    public static Font fontTitle = new Font("Century Gothic", Font.BOLD, 24);

    public SistemKasirGUI() {
        JFrame frame = new JFrame("Sistem Kasir");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);

        new HomeGUI(frame, daftarProduk);

        frame.setVisible(true);
    }
}
