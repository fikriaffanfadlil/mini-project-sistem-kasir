package frontend;

import backend.Produk;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ManajemenPersediaanGUI {
    JFrame frame;

    public ManajemenPersediaanGUI(JFrame frame, ArrayList<Produk> daftarProduk) {
        this.frame = frame;

        JPanel mainPanel = new JPanel();

        //Labels
        JLabel titleLabel = new JLabel("Manajemen Persediaan");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemKasirGUI.fontTitle);

        JLabel pilihLabel = new JLabel("Pilih Produk", SwingConstants.CENTER);
        pilihLabel.setFont(SistemKasirGUI.fontGeneral);

        //Drop down Nama Produk
        ///Membuat list nama produk
        ArrayList<String> arrayNama = new ArrayList<>();
        for (Produk m: daftarProduk) arrayNama.add(m.getNama());

        ///Setting up combo box
        JComboBox<String> comboBox = new JComboBox<>();
        comboBox.setModel(new DefaultComboBoxModel<>(arrayNama.toArray(new String[0])));

        //Buttons
        JButton buttonLihat = new JButton("Pilih");
        buttonLihat.setBackground(Color.green);
        buttonLihat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Handle if combo box is blank
                if (daftarProduk.size()<=0) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }
                else {
                    frame.remove(mainPanel);
                    new DetailProdukGUI(getProduk(daftarProduk, comboBox.getSelectedItem().toString()), frame, daftarProduk);
                }
            }
        });

        JButton buttonTambahkan = new JButton("Tambahkan Produk Baru");
        buttonTambahkan.setBackground(Color.green);
        buttonTambahkan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(mainPanel);
                new ProdukBaruGUI(frame, daftarProduk);
            }
        });

        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.setBackground(Color.pink);
        buttonKembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(mainPanel);
                new HomeGUI(frame, daftarProduk);
            }
        });

        //Adding ke panel dengan layout sesuai
        mainPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(-50,0,0,0);
        mainPanel.add(titleLabel, gbc);
        gbc.insets = new Insets(20,30,0,30);
        mainPanel.add(pilihLabel, gbc);
        mainPanel.add(comboBox,gbc);
        mainPanel.add(buttonLihat, gbc);
        gbc.insets = new Insets(30,30,0,30);
        mainPanel.add(buttonTambahkan,gbc);
        gbc.insets = new Insets(30,30,0,30);
        mainPanel.add(buttonKembali, gbc);

        frame.add(mainPanel);
        frame.repaint();
        frame.setVisible(true);
    }

    //Array search method
    public static Produk getProduk(ArrayList<Produk> daftarProduk, String nama) {
        for (Produk produk : daftarProduk) {
            if (produk.getNama().equals(nama)){
                return produk;
            }
        }
        return null;
    }
}

