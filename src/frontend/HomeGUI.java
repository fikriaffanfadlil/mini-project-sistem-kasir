package frontend;

import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import backend.*;

public class HomeGUI {
    JFrame frame;

    public HomeGUI(JFrame frame, ArrayList<Produk> daftarProduk) {
        this.frame = frame;

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Sistem Kasir");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemKasirGUI.fontTitle);

        //Inisiasi mainPanel
        JPanel mainPanel = new JPanel();

        //Inisiasi Buttons
        JButton buttonPenjualan = new JButton("Penjualan");
        JButton buttonManajemenPersediaan = new JButton("Manajemen Persediaan");
        JButton buttonLaporan = new JButton("Laporan Laba Rugi");

        //Layout
        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(-50, 0, 30, 0);

        //menambahkan Button dengan sesuai
        mainPanel.add(titleLabel, gbc);
        gbc.insets = new Insets(0, 0, 10, 0);
        mainPanel.add(buttonPenjualan, gbc);
        mainPanel.add(buttonManajemenPersediaan, gbc);
        mainPanel.add(buttonLaporan, gbc);

        //Menambahkan Listener pada Button
        ActionListener actionListener = e -> {
            String actionCommand = ((JButton) e.getSource()).getActionCommand();

            if (actionCommand.contains("Penjualan")) {
                frame.remove(mainPanel);
                new PenjualanGUI(frame, daftarProduk);
            }
            else if (actionCommand.contains("Manajemen Persediaan")) {
                frame.remove(mainPanel);
                new ManajemenPersediaanGUI(frame, daftarProduk);
            }
            else if (actionCommand.contains("Laporan Laba Rugi")) {
                frame.remove(mainPanel);
                new LaporanGUI(frame, daftarProduk);
            }
        };

        buttonPenjualan.addActionListener(actionListener);
        buttonManajemenPersediaan.addActionListener(actionListener);
        buttonLaporan.addActionListener(actionListener);

        frame.add(mainPanel);
        frame.repaint();
        frame.setVisible(true);
    }
}
